//
//  CounterSpec.swift
//  DeclarativeFizzBuzzTests
//
//  Created by vikingosegundo on 09.05.24.
//

import Quick
import Nimble

class CounterSpec:QuickSpec {
    override class func spec() {
        describe("Counter") {
            var counter:Counter!
            var fetchedValue:Int!
            let valueFetcher = { fetchedValue = $0 }
            beforeEach { counter = Counter() }
            afterEach  { counter = nil; fetchedValue = nil }
            context("newly initialized") {
                beforeEach { fetch(.value(valueFetcher), from:counter) }
                it("has count 0") { expect(fetchedValue).to(equal(0)) }
            }
            context("dec") {
                beforeEach { alter(counter, by:.decrementing(.it)) }
                beforeEach { fetch(.value(valueFetcher), from:counter) }
                it("has count -1") { expect(fetchedValue).to(equal(-1)) }
                
                context("resetting") {
                    beforeEach { alter(counter, by: .resetting(.it)) }
                    beforeEach { fetch(.value(valueFetcher), from:counter) }
                    it("has count 0") {expect(fetchedValue).to(equal(0))}
                }
            }
            context("inc") {
                beforeEach { alter(counter, by:.incrementing(.it)) }
                beforeEach { fetch(.value(valueFetcher), from:counter) }
                it("has count 1") { expect(fetchedValue).to(equal(1)) }
                
                context("inc") {
                    beforeEach { alter(counter, by:.incrementing(.it)) }
                    beforeEach { fetch(.value(valueFetcher), from:counter) }
                    it("has count 2") { expect(fetchedValue).to(equal(2)) }
                }
                context("dec") {
                    beforeEach { alter(counter, by:.decrementing(.it)) }
                    beforeEach { fetch(.value(valueFetcher), from:counter) }
                    it("has count 0") { expect(fetchedValue).to(equal(0)) }
                    
                    context("dec") {
                        beforeEach { alter(counter, by:.decrementing(.it)) }
                        beforeEach { fetch(.value(valueFetcher), from:counter) }
                        it("has count -1") {
                            expect(fetchedValue).to(equal(-1))
                        }
                    }
                }
                context("resetting") {
                    beforeEach { alter(counter, by: .resetting(.it)) }
                    beforeEach { fetch(.value(valueFetcher), from:counter) }
                    it("has count 0") {expect(fetchedValue).to(equal(0))}
                }
            }
            context("callback") {
                var callbackValue: Int!
                beforeEach { alter(counter, by:.adding(.callback{ callbackValue = $0 })) }
                afterEach  { callbackValue = nil }
                context("inc") {
                    beforeEach { alter(counter, by:.incrementing(.it)) }
                    it("callback fetches value 1") { expect(callbackValue).to(equal(1)) }
                }
                context("dec") {
                    beforeEach { alter(counter, by:.decrementing(.it)) }
                    it("callback fetches value -1") { expect(callbackValue).to(equal(-1)) }
                }
                context("doubling callback") {
                    var doublingCallbackValue: Int!
                    beforeEach { alter(counter, by:.adding(.callback{ doublingCallbackValue = 2 * $0 })) }
                    afterEach { doublingCallbackValue = nil }
                    context("inc") {
                        beforeEach { alter(counter, by:.incrementing(.it)) }
                        beforeEach { fetch(.value(valueFetcher), from:counter) }
                        it("doubling callback fetches 2") { expect(doublingCallbackValue).to(equal(2)) }
                        it("counter value is 1") { expect(fetchedValue).to(equal(1)) }
                    }
                    context("dec") {
                        beforeEach { alter(counter, by:.decrementing(.it)) }
                        beforeEach { fetch(.value(valueFetcher), from:counter) }
                        it("doubling callback fetches -2") { expect(doublingCallbackValue).to(equal(-2)) }
                        it("counter value is -1") { expect(fetchedValue).to(equal(-1)) }
                    }
                }
            }
        }
    }
}
