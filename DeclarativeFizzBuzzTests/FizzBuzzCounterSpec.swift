//
//  FizzBuzzCounterSpec.swift
//  DeclarativeFizzBuzzTests
//
//  Created by vikingosegundo on 09.05.24.
//

import Quick
import Nimble

class FizzBuzzCounterSpec: QuickSpec {
    override class func spec() {
        describe("FizzBuzzCounter") {
            var fizzBuzzCounter:Counter!
            var value:String!
            beforeEach {
                fizzBuzzCounter = Counter()
                alter(fizzBuzzCounter, by:.adding(.callback{ if ($0 %  3 == 0) && ($0 % 15 != 0){ value = "Fizz"    } }))
                alter(fizzBuzzCounter, by:.adding(.callback{ if ($0 %  5 == 0) && ($0 % 15 != 0){ value = "Buzz"    } }))
                alter(fizzBuzzCounter, by:.adding(.callback{ if ($0 % 15 == 0)                  { value = "FizzBuzz"} }))
                alter(fizzBuzzCounter, by:.adding(.callback{ if ($0 %  3 != 0) && ($0 %  5 != 0){ value = "\($0)"   } }))
            }
            afterEach {
                fizzBuzzCounter = nil
                value = nil
            }
            context( "1x increment") { beforeEach {                         alter(fizzBuzzCounter, by:.incrementing(.it))   }; it("has value 1"       ) { expect(value).to(equal("1"       )) } }
            context( "2x increment") { beforeEach { (0..<2 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 2"       ) { expect(value).to(equal("2"       )) } }
            context( "3x increment") { beforeEach { (0..<3 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context( "4x increment") { beforeEach { (0..<4 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 4"       ) { expect(value).to(equal("4"       )) } }
            context( "5x increment") { beforeEach { (0..<5 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Buzz"    ) { expect(value).to(equal("Buzz"    )) } }
            context( "6x increment") { beforeEach { (0..<6 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context( "7x increment") { beforeEach { (0..<7 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 7"       ) { expect(value).to(equal("7"       )) } }
            context( "8x increment") { beforeEach { (0..<8 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 8"       ) { expect(value).to(equal("8"       )) } }
            context( "9x increment") { beforeEach { (0..<9 ).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context("10x increment") { beforeEach { (0..<10).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Buzz"    ) { expect(value).to(equal("Buzz"    )) } }
            context("11x increment") { beforeEach { (0..<11).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 11"      ) { expect(value).to(equal("11"      )) } }
            context("12x increment") { beforeEach { (0..<12).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context("13x increment") { beforeEach { (0..<13).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 13"      ) { expect(value).to(equal("13"      )) } }
            context("14x increment") { beforeEach { (0..<14).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 14"      ) { expect(value).to(equal("14"      )) } }
            context("15x increment") { beforeEach { (0..<15).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value FizzBuzz") { expect(value).to(equal("FizzBuzz")) } }
            context("16x increment") { beforeEach { (0..<16).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 16"      ) { expect(value).to(equal("16"      )) } }
            context("17x increment") { beforeEach { (0..<17).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 17"      ) { expect(value).to(equal("17"      )) } }
            context("18x increment") { beforeEach { (0..<18).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context("19x increment") { beforeEach { (0..<19).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 19"      ) { expect(value).to(equal("19"      )) } }
            context("20x increment") { beforeEach { (0..<20).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Buzz"    ) { expect(value).to(equal("Buzz"    )) } }
            context("21x increment") { beforeEach { (0..<21).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context("22x increment") { beforeEach { (0..<22).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 22"      ) { expect(value).to(equal("22"      )) } }
            context("23x increment") { beforeEach { (0..<23).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 23"      ) { expect(value).to(equal("23"      )) } }
            context("24x increment") { beforeEach { (0..<24).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context("25x increment") { beforeEach { (0..<25).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Buzz"    ) { expect(value).to(equal("Buzz"    )) } }
            context("26x increment") { beforeEach { (0..<26).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 26"      ) { expect(value).to(equal("26"      )) } }
            context("27x increment") { beforeEach { (0..<27).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value Fizz"    ) { expect(value).to(equal("Fizz"    )) } }
            context("28x increment") { beforeEach { (0..<28).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 28"      ) { expect(value).to(equal("28"      )) } }
            context("29x increment") { beforeEach { (0..<29).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value 29"      ) { expect(value).to(equal("29"      )) } }
            context("30x increment") { beforeEach { (0..<30).forEach { _ in alter(fizzBuzzCounter, by:.incrementing(.it)) } }; it("has value FizzBuzz") { expect(value).to(equal("FizzBuzz")) } }
        }
    }
}
