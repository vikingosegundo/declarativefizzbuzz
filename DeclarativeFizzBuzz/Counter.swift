//
//  Counter.swift
//  DeclarativeFizzBuzz
//
//  Created by vikingosegundo on 09.05.24.
//

// MARK: - Properties
class Counter {
    private var value: Int = 0 { didSet { execute(.callbacks) } }
    private var callbacks: [(Int) -> ()] = []
}

//MARK: - Command
extension Counter {
    enum Command {
        enum It {
            case it
        }
        case incrementing(It)
        case decrementing(It)
        case resetting(It)
        case adding(Adding); enum Adding {
            case callback((Int) -> ())
        }
    }
    fileprivate func alter(by cmd:Command) {
        switch cmd {
        case     .incrementing(.it)    : value += 1
        case     .decrementing(.it)    : value -= 1
        case     .resetting(.it)       : value = 0
        case let .adding(.callback(cb)): callbacks += [cb]
        }
    }
}
func alter(_ counter:Counter, by change:Counter.Command) { counter.alter(by: change) }

//MARK: - Fetch
extension Counter {
    enum Fetch {
        case value((Int) -> ())
        case formattedValue((String) -> ())
    }
    fileprivate func fetch(_ fetch:Fetch) {
        switch fetch {
        case let .value(f): f(value)
        case let .formattedValue(f): f("> \(value)! <")
        }
    }
}
func fetch(_ fetch:Counter.Fetch, from counter:Counter) { counter.fetch(fetch) }

//MARK: - Execute
extension Counter {
    enum Execute {
        case callbacks
    }
    fileprivate func execute(_ exe:Execute) {
        switch exe {
        case .callbacks: callbacks.forEach { $0(value) } }
    }
}
func execute(_ exe:Counter.Execute, on counter:Counter) { counter.execute(exe) }
