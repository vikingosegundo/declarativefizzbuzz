//
//  main.swift
//  DeclarativeFizzBuzz
//
//  Created by vikingosegundo on 09.05.24.
//

let fizzBuzzCounter = Counter()

alter(fizzBuzzCounter, by: .adding(.callback{ if ($0 %  3 == 0) && ($0 % 15 != 0){ print("Fizz"    ) } }))
alter(fizzBuzzCounter, by: .adding(.callback{ if ($0 %  5 == 0) && ($0 % 15 != 0){ print("Buzz"    ) } }))
alter(fizzBuzzCounter, by: .adding(.callback{ if ($0 % 15 == 0)                  { print("FizzBuzz") } }))
alter(fizzBuzzCounter, by: .adding(.callback{ if ($0 %  3 != 0) && ($0 %  5 != 0){ print("\($0)"   ) } }))

(0..<32).forEach { _ in
    alter(fizzBuzzCounter, by: .incrementing(.it))
}

fetch(.value({ print($0) }), from: fizzBuzzCounter)
fetch(.formattedValue({ print($0) }), from: fizzBuzzCounter)
execute(.callbacks, on: fizzBuzzCounter)
